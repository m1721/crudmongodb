## Actuator url 
```url
http://localhost:8080/wissen/mongodbCrud/health
```

## Create   
```url
localhost:8080/user/register
```
### request
```json
 {
        "id": 6,
        "name": "manuj",
        "age": 23,
        "dob": "12-29-1999",
        "email": "manuj2912@gmail.com"
    }
```

## Retrive
```url
localhost:8080/user/fetch-all
```
### response
```json
    {
    {
        "id": 6,
        "name": "manuj",
        "age": 23,
        "dob": "12-29-1999",
        "email": "manuj2912@gmail.com"
    },
    {
        "id": 7,
        "name": "anuj",
        "age": 20,
        "dob": "12-29-2000",
        "email": "anuj2912@gmail.com"
    }
    
    
    }
```

## Update
```url
localhost:8080/user/update
```
### request

```json
    {
        "id": 7,
        "name": "anuj",
        "age": 20,
        "dob": "12-29-2000",
        "email": "anuj2912@gmail.com"
    }
```

## Delete
```url
localhost:8080/user/remove?id=111
```
