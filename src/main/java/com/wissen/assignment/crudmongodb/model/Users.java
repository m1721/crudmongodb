package com.wissen.assignment.crudmongodb.model;


import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
@Getter
@Setter
public class Users {

    @Id
    private String id;
    private String name;
    private Integer age;
    private Date dob;
    private String email;

}
