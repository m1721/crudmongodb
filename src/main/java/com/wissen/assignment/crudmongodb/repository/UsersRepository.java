package com.wissen.assignment.crudmongodb.repository;

import com.wissen.assignment.crudmongodb.model.Users;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends MongoRepository<Users,String> {
}
