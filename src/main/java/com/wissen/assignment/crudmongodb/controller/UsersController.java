package com.wissen.assignment.crudmongodb.controller;

import com.wissen.assignment.crudmongodb.logConfig.LogRequestResponse;
import com.wissen.assignment.crudmongodb.model.Users;
import com.wissen.assignment.crudmongodb.request.RegisterRequest;
import com.wissen.assignment.crudmongodb.service.UsersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UsersController {

    private static final Logger logger = LoggerFactory.getLogger(UsersController.class);

    UsersService usersService;

    @Autowired
    public UsersController(UsersService usersService){
        this.usersService = usersService;
    }

    @LogRequestResponse
    @GetMapping(value = "/fetch-all",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity fetchAll(){
        List<Users> users = usersService.fetchAll();
        return ResponseEntity.status(HttpStatus.OK).body(users);
    }

    @LogRequestResponse
    @PostMapping(value = "/register",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity registerUser(@Valid @RequestBody RegisterRequest registerRequest) throws ParseException {
        usersService.registerUser(registerRequest);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @LogRequestResponse
    @PostMapping(value = "/update",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updateUser(@Valid @RequestBody RegisterRequest registerRequest) throws ParseException {
        usersService.registerUser(registerRequest);
        return ResponseEntity.status(HttpStatus.OK).build();
    }


    @LogRequestResponse
    @DeleteMapping(value = "/remove",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity removeUser(@Valid @RequestParam(("id")) String id) throws ParseException {
        Assert.notNull(id,"For deletion id is not null");
        usersService.deleteUser(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
