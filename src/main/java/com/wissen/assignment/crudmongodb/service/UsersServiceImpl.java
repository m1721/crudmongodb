package com.wissen.assignment.crudmongodb.service;

import com.mongodb.MongoException;
import com.wissen.assignment.crudmongodb.helper.ApplicationConstants;
import com.wissen.assignment.crudmongodb.model.Users;
import com.wissen.assignment.crudmongodb.repository.UsersRepository;
import com.wissen.assignment.crudmongodb.request.RegisterRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class UsersServiceImpl implements UsersService {

    UsersRepository usersRepository;

    @Autowired
    public UsersServiceImpl(UsersRepository usersRepository){
        this.usersRepository = usersRepository;

    }

    @Override
    public List<Users> fetchAll() throws MongoException {
        return usersRepository.findAll();
    }

    @Override
    public void registerUser(RegisterRequest registerRequest) throws MongoException, ParseException {
        Users users = new Users();
        users.setAge(Integer.parseInt(registerRequest.getAge()));
        users.setEmail(registerRequest.getEmail());
        users.setDob(stringToDate(registerRequest.getDob(), ApplicationConstants.DATE_FORMAT_DD_MM_YYYY));
        users.setName(registerRequest.getName());
        users.setId(registerRequest.getId());
        usersRepository.save(users);
    }

    @Override
    public void deleteUser(String id) throws MongoException {
        if(usersRepository.existsById(id)){
            usersRepository.deleteById(id);
        }
    }

    public static Date stringToDate(String date , String dateFormat) throws ParseException {
        DateFormat sdf = new SimpleDateFormat(dateFormat);
        return sdf.parse(date);
    }
}
