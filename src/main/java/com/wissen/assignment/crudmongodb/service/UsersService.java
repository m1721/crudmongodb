package com.wissen.assignment.crudmongodb.service;

import com.mongodb.MongoException;
import com.wissen.assignment.crudmongodb.model.Users;
import com.wissen.assignment.crudmongodb.request.RegisterRequest;

import java.text.ParseException;
import java.util.List;

public interface UsersService {
    public List<Users> fetchAll() throws MongoException;
    public void registerUser(RegisterRequest registerRequest) throws MongoException, ParseException;
    public void deleteUser(String id) throws MongoException;
}
