package com.wissen.assignment.crudmongodb;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.wissen.assignment.crudmongodb.*"})
public class CrudmongodbApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudmongodbApplication.class, args);
	}

}
