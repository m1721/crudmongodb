package com.wissen.assignment.crudmongodb.helper;

public class ApplicationConstants {

    public static final String IS_REQUIRED = " is required";
    public static final String MUST_BE_POSITIVE_NUMBER = " must be a positive number";


    public  static  final  String DATE_DD_MM_YYYY="\\d{2}-\\d{2}-\\d{4}$";
    public static final String REG_EX_STRICTLY_CHARACTERS ="^[a-zA-Z]*$";
    public static final String  REG_EX_EMAIL = "^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";
    public static final String DATE_FORMAT_DD_MM_YYYY ="dd-mm-yyyy";
}
