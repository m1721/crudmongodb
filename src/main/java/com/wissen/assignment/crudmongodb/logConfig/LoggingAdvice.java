package com.wissen.assignment.crudmongodb.logConfig;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAdvice {
    protected static final Logger LOGGER = LoggerFactory.getLogger(LoggingAdvice.class);

    @Around("@annotation(com.wissen.assignment.crudmongodb.logConfig.LogRequestResponse)")
    public Object logRequestResponse(ProceedingJoinPoint proceedingJoinPoint) throws Throwable{
        ObjectWriter objectWriter = new ObjectMapper().writerWithDefaultPrettyPrinter();
        String methodName = proceedingJoinPoint.getSignature().getName();
        String className = proceedingJoinPoint.getTarget().getClass().toString();
        Object[] objects = proceedingJoinPoint.getArgs();
        long startTime = System.currentTimeMillis();
        LOGGER.info("Invoked "+className+"::"+methodName+" ARG - " + objectWriter.writeValueAsString(objects));
        Object object = proceedingJoinPoint.proceed();
        long endTime = System.currentTimeMillis();
        long methodExecutionTime = endTime-startTime;
        LOGGER.info("End "+className+":"+methodName+" Execution in millisec "+methodExecutionTime + " RESPONSE - "+objectWriter.writeValueAsString(object));
        return object;
    }
}
