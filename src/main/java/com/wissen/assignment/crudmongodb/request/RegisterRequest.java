package com.wissen.assignment.crudmongodb.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;

import static com.wissen.assignment.crudmongodb.helper.ApplicationConstants.*;
@Getter
@Setter
public class RegisterRequest {
    
    @Positive(message = "id"+MUST_BE_POSITIVE_NUMBER)
    @NotNull(message = "id"+IS_REQUIRED)
    private String id;

    @NotNull(message = "name"+IS_REQUIRED)
    @Pattern(regexp = REG_EX_STRICTLY_CHARACTERS,message = "name is characters only")
    private String name;

    @NotNull(message = "age"+IS_REQUIRED)
    @Positive(message = "age"+MUST_BE_POSITIVE_NUMBER)
    private String age;

    @NotNull(message = "dob"+IS_REQUIRED)
    @Pattern(regexp = DATE_DD_MM_YYYY,message = "dob format id dd-mm-yyyy ")
    private String dob;

    @NotNull(message = "email"+IS_REQUIRED)
    @Pattern(regexp = REG_EX_EMAIL,message = "email format is abc@xyz.com")
    private String email;

}
